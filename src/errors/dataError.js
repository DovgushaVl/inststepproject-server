export default class BadDataError
    extends Error{
    constructor(message,status) {
        super(message,status);
        this.status = status;
    }
}