export default class BadDataError
    extends Error{
    constructor(message) {
        super(message);
        this.status = 404;
    }
}