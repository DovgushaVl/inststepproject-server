import Router from 'express';
import accountModel from "../../db/models/accountModel";
import emailStatus from '../middlewares/emailStatus'
import dataCheck from '../middlewares/dataCheck';
import nickNameStatus from '../middlewares/nickNameStatus'
import generateToken from "../../services/generateToken";
import loginService from "../middlewares/loginService";
import currentUser from "../middlewares/currentUser";
import isAuth from "../middlewares/isAuth";
import webToken from 'jsonwebtoken';

const route = Router();
export default (app) => {
    app.use('/auth', route);
    route.post('/login', loginService, async (req, res, next) => {
        try {
            const _Token = res._Token;
            res.json({_Token});
        } catch (e) {
            next(e);
        }
    });
    route.post('/signup', dataCheck,
        async (req, res, next) => {
            try {
                const data = res.verified;
                console.log('dataAuth', data);
                const account = accountModel({...data});
                const _Token = await generateToken({user: account});
                await account.save(function (err) {
                    if (err) {
                        return res.json({err: err})
                    }
                    return res.json({
                        account: account,
                        _Token,
                        text: 'Аккаунт сохранен'
                    });
                });
            } catch (e) {
                next(e);
            }
        });
    route.get('/emailcheck', emailStatus,
        async (req, res, next) => {
            try {
                const checkResult = res.result;
                const message = checkResult.message;
                const status = checkResult.status;
                res.json({message, status});
            } catch (e) {
                next(e)
            }
        });
    route.get('/nicknamecheck', nickNameStatus,
        async (req, res, next) => {
            try {
                const message = res.result.message;
                const status = res.result.status;
                res.json({message, status});
            } catch (e) {
                next(e)
            }
        });
};