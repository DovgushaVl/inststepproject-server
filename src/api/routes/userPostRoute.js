import {Router} from 'express';
import isAuth from "../middlewares/isAuth";
import currentUser from "../middlewares/currentUser";
import findUserPost from '../../services/findUserPosts';
import addPostService from "../../services/addPostService";
import likeService from '../../services/likeService'

const route = Router();

export default (app) => {
    app.use('/posts', route);
    route.post('/newpost', isAuth, currentUser, async (req, res, next) => {
        try {
            const currentUser = req.currentUser;
            const currentUserId = currentUser._id;
            const reqData = req.body;
            const date = new Date;
            const data = {...reqData, ...{author: currentUserId},...{date:
            date.toLocaleString()}};
            const addPost = await addPostService(data);
            await addPost.save(function (err) {
                if (err) {
                    return res.json({err: err})
                }
                return res.json({
                    postId: addPost,
                    text: 'Пост сохранен'
                });
            });
        } catch (e) {
            next(e);
        }
    });
    route.post('/liked',
        isAuth,
        currentUser,
        async (req, res, next) => {
            try {
                const currentUserId = req.currentUser.id;
                const target = req.body;
                const data = {currentUserId, ...target};
                const like = await likeService(data);
                console.log(like);
                res.json({message:like.message});
            } catch (e) {
                next(e);
            }
        })
}