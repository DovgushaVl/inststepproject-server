import {Router} from 'express';
import isAuth from "../middlewares/isAuth";
import currentUser from "../middlewares/currentUser";
import accountModel from "../../db/models/accountModel";
import BadDataError from '../../errors/dataError'
import postModel from "../../db/models/postModel";

const route = Router();

export default (app) => {
    app.use('/', route);
    route.get('/news', isAuth, currentUser, async (req, res, next) => {
        const currentUser = req.currentUser;
    });
    route.get('/userdata', isAuth, currentUser, async (req, res, next) => {
        try {
            if (!req.headers._id) {
                const currentUser = req.currentUser;
                return res.json({
                    _id: currentUser._id,
                    firstName: currentUser.firstName,
                    lastName: currentUser.lastName,
                    nickName: currentUser.nickName,
                    avatar: currentUser.avatar,
                    subscribes: currentUser.subscribes,
                    subscribers: currentUser.subscribers
                });
            }
            const _id = req.headers._id;
            const user = await accountModel.findById(_id);
            res.json({
                _id: user._id,
                firstName: user.firstName,
                lastName: user.lastName,
                nickName: user.nickName,
                avatar: user.avatar,
                subscribes: user.subscribes,
                subscribers: user.subscribers
            });
        } catch (e) {
            next(e)
        }
    });
    route.post('/:userId/subscribers',
        isAuth,
        currentUser,
        async (req, res, next) => {
            try {
                const {userId} = req.params;
                const currentUser = req.currentUser;
                const currentUserId = currentUser._id;
                console.log(currentUserId);
                if (userId === currentUserId.toString()) {
                    throw new BadDataError('Вы не можете подписаться сами на себя');
                }
                const user = await accountModel.findById(userId);
                const curUser = await accountModel.findById(currentUserId);
                if (user === null) {
                    throw new BadDataError('Нет пользователя с таким ID', 400)
                }
                const subscribes = curUser.subscribes;
                const subscribers = user.subscribers;
                if (subscribers.indexOf(currentUserId) === -1 && subscribes.indexOf(userId) === -1) {
                    subscribers.push(currentUserId);
                    subscribes.push(userId);
                    console.log(subscribes);
                    console.log(subscribers);
                    await curUser.save();
                    await user.save();
                    return res.json({message: "Вы подписались на обновления этого пользователя"});
                }
                console.log('subscribers', subscribers);
                console.log('subscribes', subscribes);
                const subscribersIndex = subscribers.indexOf(currentUserId);
                const subscribesIndex = subscribes.indexOf(userId);
                console.log(subscribesIndex);
                subscribes.splice(subscribesIndex, 1);
                subscribers.splice(subscribersIndex, 1);
                console.log(subscribes);
                await curUser.save();
                await user.save();
                return res.json({message: 'Вы отписались от обновлений этого пользователя'});
            } catch (e) {
                next(e);
            }
        });
    route.get('/:userId/posts',
        isAuth,
        currentUser,
        async (req, res, next) => {
            try {
                const {userId} = req.params;
                const currentUser = res.currentUser;
                const user = await accountModel.findById(userId);
                if (user === null) {
                    throw new BadDataError("Нет пользователя с переданным вами ID", 400)
                }
                const posts = await postModel.find({author: userId});
                res.json({posts})
            } catch (e) {
                next(e);
            }
        })
};