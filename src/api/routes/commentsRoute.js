import {Router} from 'express';
import postModel from "../../db/models/postModel";
import isAuth from "../middlewares/isAuth";
import currentUser from "../middlewares/currentUser";
const route = Router();
export default (app) => {
    app.use('/comments', route);
    route.post('/newcomment', isAuth, currentUser,async (req, res, next) => {
        try {
            const data = req.body;
            const currentUser = req.currentUser;
            const postId = {_id: data.postId};
            console.log(data);
            console.log(currentUser);
            const date = new Date;
            const comment = {
                author:{
                    firstName:currentUser.firstName,
                    lastName:currentUser.lastName,
                    nickName:currentUser.nickName,
                    avatar:currentUser.avatar,
                    _id:currentUser._id
                },
                commentary:{text:data.text,
                date:date.toLocaleString()}
            };
            const addComentary = await postModel.findOne(postId);
            const commentaries = addComentary.commentaries;
            commentaries.push(comment);
            await addComentary.save();
            // const post = await postModel.findById(data.postId);
            res.json({message:'Коментарий успешно добавлен'})
        } catch (e) {
            next(e);
        }
    });
};