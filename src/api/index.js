import {Router} from 'express';
import authRoute from "./routes/auth";
import userPostRoute from "./routes/userPostRoute";
import commentsRoute from "./routes/commentsRoute";
import userRoute from "./routes/userRoute";

export default () => {
    const app = Router();
    authRoute(app);
    userRoute(app);
    userPostRoute(app);
    commentsRoute(app);
    return app;
}