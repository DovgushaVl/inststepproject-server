import accountModel from "../../db/models/accountModel";
import BadDataError from "../../errors/dataError";
import checkEmailAvaliability from "../../services/checkEmailAvaliability";
import emailValidator from "../../services/emailValidator";

export default async function emailStatus(req, res, next) {
    try {
        const data = req.headers;
        console.log(data);
        if (!req.headers.email){
            throw new BadDataError('Не передан email', 400)
        }
        const validateEmail = await emailValidator(data);
        if (validateEmail === true) {
            const emailAviliable = await checkEmailAvaliability(data);
            if (emailAviliable === true) {
                res.result = {message: 'Email свободен', status: 200};
            } else {
                throw new BadDataError('На этот email уже зарегистрирован аккаунт', 400)
            }
        } else {
            throw new BadDataError('Неверный email', 400)

        }
        return next();
    } catch (e) {
        next(e)
    }
}

