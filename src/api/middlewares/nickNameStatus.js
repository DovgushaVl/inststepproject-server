import BadDataError from "../../errors/dataError";
import nickNameValidator from "../../services/nickNameValidator";
import checkNicknameAvaliability from "../../services/checkNicknameAvaliability";

export default async function (req, res, next) {
    try {
        const data = req.headers;
        const nickName = data.nickname;
        if (!nickName) {
            throw new BadDataError('nickName не передан', 400)
        }
        const validateNickName = await nickNameValidator(nickName);
        if (validateNickName !== true) {
            throw new BadDataError('Слишком короткий nickName', 400)
        }
        const nickNameAvaliability = await checkNicknameAvaliability(nickName);
        if (nickNameAvaliability !== true) {
            throw new BadDataError('Этот никнейм уже занят', 400);
        }
        res.result = {message: 'Никнейм свободен', status: 200};
        return next();
    } catch (e) {
        next(e)
    }
}