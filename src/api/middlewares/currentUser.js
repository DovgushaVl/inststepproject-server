import accountModel from "../../db/models/accountModel";
import BadDataError from "../../errors/dataError";
export default async function (request, response, next) {
    try {
        // console.log(request);
        if (!request.token){
            throw new BadDataError('Token is not set');
        }

        const user = await accountModel.findById(request.token._id);
        if (!user) {
            throw new BadDataError('Token is expired OR user was not found!');
        }

        request.currentUser = user;

        return next();
    } catch (e) {
        return next(e);
    }

}
