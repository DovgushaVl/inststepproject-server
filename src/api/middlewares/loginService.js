import BadDataError from "../../errors/dataError";
import emailValidator from "../../services/emailValidator";
import passwordValidator from "../../services/passwordValidator";
import checkLoginData from "../../services/checkLoginData";

export default async function (req, res, next) {
    try {
        const data = req.body;
        const emailValid = await emailValidator(data);
        const passwordValid = await passwordValidator(data);
        if (emailValid !== true) {
            throw new BadDataError('Неверный email, попробуйте еще раз', 400)
        }
        if (passwordValid !== true) {
            throw new BadDataError('Слишком короткий пароль, возможно вы ошиблись', 400)
        }
        const token = await checkLoginData(data);
        if (token === false){
            throw new BadDataError('Неверный логин или пароль', 400)
        }
        res._Token = token;
        return next()
    } catch (e) {
        next(e);
    }
}