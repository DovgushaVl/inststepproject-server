export default function (err, req, res, next) {
    res
        .status(err.status || 500)
        .json({
            error: {
                message: err.message,
                status: err.status
            }
        })
}