import checkEmailAvaliability from '../../services/checkEmailAvaliability'
import accountModel from "../../db/models/accountModel";
import emailValidator from "../../services/emailValidator";
import BadDataError from "../../errors/dataError";
import checkNicknameAvaliability from "../../services/checkNicknameAvaliability";

export default async function (req, res, next) {
    try {
        const data = req.body;
        if (!data.firstName || !data.lastName || !data.nickName || !data.email || !data.password) {
            throw new BadDataError('Missed mandatory field', 400);
        }
        const validateEmail = await emailValidator(data);
        if (validateEmail !== true) {
            throw new BadDataError('Поле Email заполнено неверно', 400)
        } else {
            const emailAvaliable = await checkEmailAvaliability(data);
            if (emailAvaliable !== true) {
                throw new BadDataError('На этот email уже зарегистрирован аккаунт', 400)
            }
            const unverifiedData = {
                email: data.email,
                firstName: data.firstName,
                lastName: data.lastName,
                nickName: data.nickName,
                password: data.password
            };
            const templateData = {
                firstName: {
                    minLength: 1
                },
                email: {
                    minLength: 5
                },
                password: {minLength: 8},
                lastName: {
                    minLength: 1
                },
                nickName: {
                    minLength: 5
                }

            };
            for (let dataField in unverifiedData) {
                if (unverifiedData[dataField].length < templateData[dataField].minLength) {
                    throw new BadDataError(`Поле : [${dataField}] заполнено неверно`, 400)
                }
            }
            const nickNameAvaliable = await checkNicknameAvaliability(data.nickName);
            if (nickNameAvaliable !== true) {
                throw new BadDataError('Этот Никнейм уже занят', 400);
            }
            const verifiedData = {...unverifiedData};
            verifiedData.avatar = '';
            res.verified = verifiedData;
            return next();
        }
    } catch (e) {
        next(e)
    }
}