import jwt from 'express-jwt';
import config from '../../config';

const isAuth = jwt({
    secret: config.securityToken,
    userProperty: 'token',
    getToken: (request) => {
        if (
            request.headers.authorization &&
            (
                request.headers.authorization.split(' ')[0] === 'Token' ||
                request.headers.authorization.split(' ')[0] === 'Bearer'
            )
        ) {
            return request.headers.authorization.split(' ')[1];
        }

        return null;
    }
});

export default isAuth;