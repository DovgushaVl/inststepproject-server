import expressLoader from './express';
import dbLoader from "./dbLoader";

export default async ({expressApp}) => {
    await expressLoader({app:expressApp});
    console.log('ExpressReady');
    await dbLoader();
    console.log('MongoReady');
}