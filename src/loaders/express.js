import routes from '../api';
import bodyParser from 'body-parser';
import generalError from "../api/middlewares/generalError";
import cors from 'cors';
export default ({app}) => {
    app.use(cors());
    app.options('*', cors);
    app.set('view engine', 'pug');
    app.set('views', 'src/templates');
    app.use(bodyParser.json());
    app.use('/', routes());
    app.use(generalError);
};