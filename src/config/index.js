import dotenv from 'dotenv';
process.env.NODE_ENV = process.env.NODE_ENV || 'development';

const envFound = dotenv.config();
console.log(envFound);
if (!envFound) {
    throw new Error(`No .env file`);
}

export default {
    port: parseInt(process.env.PORT, 10),
    databaseURL: process.env.MONGODB_URI,
    securityToken: process.env.SECURITY_TOKEN
}