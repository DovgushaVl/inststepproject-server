import * as EmailValidator from "email-validator";

export default async function (data) {
    const email = data.email;
    return EmailValidator.validate(email);
}