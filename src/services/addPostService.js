import postModel from "../db/models/postModel";
import BadDataError from "../errors/dataError";

export default async function (data) {
    const postData = data;
    if (!postData.author || !postData.photo || !postData.title) {
        throw new Error('missed mandatory field',400);
    }
    if (postData.photo.length < 1 || postData.title < 1) {
        throw new BadDataError('Не передана фотография либо ее описание')
    }
    const newPost = new postModel({...postData});
    return newPost;
}