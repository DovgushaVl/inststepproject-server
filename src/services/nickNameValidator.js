export default async function (data) {
    const nickName = data;
    if (nickName.length < 5) {
        return false;
    }
    return true;
}