import accountModel from "../db/models/accountModel";
import generateToken from "./generateToken";
export default async function (data) {
    try {
        const email = data.email;
        const password = data.password;
        const account = await accountModel.findOne({email:email});
        if (!account){
            return false;
        }if (account.password === password){
            const token = await generateToken({user:account});
            return token;
        }return false
    }catch (e) {
        console.log(e)
    }
}