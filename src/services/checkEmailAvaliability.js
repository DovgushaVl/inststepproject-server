import accountModel from "../db/models/accountModel";
import emailValidator from "./emailValidator";

export default async function (data) {
    try {
        const email = data.email;
        const avaliability = await accountModel.find({email: email});
        if (avaliability.length !== 0) {
            return false;
        }return true;

    }catch (e) {
        console.log(e);
    }
}