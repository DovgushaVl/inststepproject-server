import BadDataError from "../errors/dataError";
export default async function (data) {
    if(!data.password){
        throw new BadDataError("Не передан пароль",400)
    }
    const password = data.password;
    if (password.length < 8){
        return false
    }
    return true;
}