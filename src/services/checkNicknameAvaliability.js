import * as EmailValidator from "email-validator";
import accountModel from "../db/models/accountModel";

export default async function (data) {
    try {
        const nickName = data;
        const avaliability = await accountModel.find({nickName: nickName});
        if (avaliability.length !== 0) {
            return false;
        }return true;

    }catch (e) {
        console.log(e);
    }
}