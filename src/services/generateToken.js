import jwt from "jsonwebtoken";
import config from '../config/index';
export default function generateToken({user}) {
    const today = new Date();
    const exp = new Date(today);
    exp.setDate(today.getDate() + 60);
    console.log(user);
    const token = jwt.sign({
        _id: user._id,
        password: user.password,
        login: user.name,
        email:user.email,
        nickName : user.nickName,
        firstName:user.firstName,
        lastName:user.lastName,
        avatar:user.avatar,
        exp: exp.getTime() / 100
    }, config.securityToken);
    return token;
}