import postModel from "../db/models/postModel";
import BadDataError from "../errors/dataError";

export default async function (data) {
    const post = {_id: data.postId};
    if (!post._id){
        throw new BadDataError('Не передан id поста')
    }
    const userId = data.currentUserId;
    const addLike = await postModel.findById(post);
    if (addLike === null) {
        throw new BadDataError('Такого поста с таким id не найдено !', 400)
    }
    const likes = addLike.likes;
    if (likes.indexOf(userId) === -1) {
        likes.push(userId);
        await addLike.save();
        // const as = await postModel.findById(post);
        // console.log('like',as.likes);
        return {message:'Вы лайкнули эту запись !'}
    }
    const index = likes.indexOf(userId);
    likes.splice(index,1);
    await addLike.save();
    // const as = await postModel.findById(post);
    // console.log('unlike',as.likes);
    return {message:'Эта запись вам больше не нравится !'}
}