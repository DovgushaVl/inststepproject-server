import express from 'express';
import loader from './loaders/index';
import config from './config/index';

async function startServer() {
    const port = config.port;
    const app = express();
    await loader({expressApp:app});    app.listen(port, err => {
        if (err) {
            process.exit(1);
            return
        }
        console.log(`❖❖❖❖❖❖❖❖❖❖❖❖❖❖❖❖❖❖❖❖❖❖❖❖❖❖❖❖❖❖❖❖❖❖❖❖❖❖❖❖❖❖❖❖❖
        Server listening on port: ${port || 5000}
❖❖❖❖❖❖❖❖❖❖❖❖❖❖❖❖❖❖❖❖❖❖❖❖❖❖❖❖❖❖❖❖❖❖❖❖❖❖❖❖❖❖❖❖❖`
        );
    });
}
startServer();