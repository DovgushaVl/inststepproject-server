import mongoose from 'mongoose';

const accountSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
        required: true
    },
    nickName: {type: String, required: true},
    birthDate: Date,
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    subscribers: Array,
    subscribes: Array,
    avatar: String
});

export default mongoose.model('Account', accountSchema)