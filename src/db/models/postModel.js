import mongoose from 'mongoose';

const postSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    date: {type:Date, default:Date.now()},
    photo: {
        type: String,
        required: true
    },
    author: mongoose.Schema.Types.ObjectID,
    commentaries: Array,
    likes: Array
});

export default mongoose.model('Post', postSchema)