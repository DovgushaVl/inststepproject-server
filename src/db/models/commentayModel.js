import mongoose from 'mongoose';

const accountSchema = new mongoose.Schema({
    author : UserID,
    data: Date,
    text: String
});

export default mongoose.model('Account', accountSchema)